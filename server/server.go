package main

import (
	"encoding/json"
	"log"
	"os"
	"net"
	"fmt"
)

type In struct {
	Numbers []int `json:"numbers"`
	K int `json:"k"`
}

type Out struct {
	Sum int `json:"sum"`
}

func gcd(a, b int) int {
    for b != 0 {
        a, b = b, a % b
    }

    return a
}

func rotateL(a []int, i int) {
    i = i % len(a)
    if i < 0 {
        i += len(a)
    }

    for c := 0; c < gcd(i, len(a)); c++ {
        t := a[c]
        j := c
        for {
            k := j + i
            // loop around if we go past the end of the slice
            if k >= len(a) {
                k -= len(a)
            }
            // end when we get to where we started
            if k == c {
                break
            }
            // move the element directly into its final position
            a[j] = a[k]
            j = k
        }
        a[j] = t
    }
}

func rotateR(a []int, i int) {
    rotateL(a, len(a) - i)
}

func decomposeNumber(number int) []int {
  if number == 0 {
    return []int{0}
  }

	array := []int{}

	for {
		if number == 0 {
			return array
		}
		digit := number % 10
		number = number / 10
		array = append([]int{digit}, array...)
	}
}

func recomposeNumber(numbers []int) int {
  final := 0
  multiplier := 1
  max := len(numbers) - 1
  for index, _ := range numbers {
    final += numbers[max - index] * multiplier
    multiplier *= 10
  }

  return final
}

func calculateSum(array[] int, K int) int {
  sum := 0
	for _, element := range array {
    decomposed := decomposeNumber(element)
    rotateR(decomposed, K)
    recomposed := recomposeNumber(decomposed)
    sum += recomposed
	}

	return sum
}

func handleConnection(conn net.Conn, clientId int) {
	log.Println("client with id", clientId, "has connected")

	decoder := json.NewDecoder(conn)
	encoder := json.NewEncoder(conn)
	var msgIn In
	var msgOut Out

	for {
		if err := decoder.Decode(&msgIn); err != nil {
			log.Println(err, msgIn)
			break
		}
		log.Println("client with id", clientId, "requested sum for", msgIn.Numbers)

		if len(msgIn.Numbers) > 0 {
			log.Println("server is processing", msgIn.Numbers, "array")
			msgOut.Sum = calculateSum(msgIn.Numbers, msgIn.K)
			log.Println("sending response to client with id", clientId)
			if err := encoder.Encode(msgOut); err != nil {
				log.Println(err, msgOut)
				break
			}
			log.Println("client with id", clientId, "received", msgOut.Sum, "sum response")
		}
	}

	conn.Close()
	log.Println("disconnected from client with id", clientId)
}

func main() {

	var PORT string
	if PORT = os.Getenv("PORT"); PORT == "" {
		PORT = "3000"
	}

	var clientId int = 0
	server, err := net.Listen("tcp", fmt.Sprintf(":%s", PORT))
	log.Println("server binded on port", PORT)

	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	defer func() {
		server.Close()
		log.Println("server closed")
	}()

	for {
		conn, err := server.Accept()
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}

		go handleConnection(conn, clientId)
		clientId += 1
	}
}