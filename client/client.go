package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"os"
	"strconv"
	"time"
)

type Config struct {
	Host           string `json:"host"`
	Port           int    `json:"port"`
	RandomizeArray bool   `json:"randomizearray`
	Array          []int  `json:"array"`
	K              int    `json:k`
}

type Out struct {
	Numbers []int `json:"numbers"`
	K       int   `json:"k"`
}

type In struct {
	Sum int `json:"sum"`
}

func generateRandomArray() ([]int, int) {
	k := rand.Intn(8)
	var array = make([]int, 5)

	for i := 0; i < 5; i++ {
		array[i] = rand.Intn(1000000)
	}

	return array, k
}

func main() {
	jsonFile, err := os.Open("config.json")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)
	var config Config
	json.Unmarshal(byteValue, &config)
	jsonFile.Close()

	var PORT, HOST string
	if PORT = os.Getenv("PORT"); PORT != "" {
		p, err := strconv.Atoi(PORT)
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
		config.Port = p
	}
	if HOST = os.Getenv("HOST"); HOST != "" {
		config.Host = HOST
	}

	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", config.Host, config.Port))
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	log.Println(fmt.Sprintf("connected to %s:%d", config.Host, config.Port))
	defer conn.Close()

	decoder := json.NewDecoder(conn)
	encoder := json.NewEncoder(conn)

	var msgIn In
	var msgOut Out

	computeOnServer := func(msgOut Out) {
		log.Println("sending array", msgOut.Numbers, "with K =", msgOut.K, "to server")
		if err := encoder.Encode(msgOut); err != nil {
			log.Fatal(err, msgOut)
			os.Exit(1)
		}
		log.Println("waiting for answer")
		if err := decoder.Decode(&msgIn); err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
		log.Println("Sum for", msgOut.Numbers, "with K =", msgOut.K, "is", msgIn.Sum)
	}

	if config.RandomizeArray == true {
		rand.Seed(time.Now().UnixNano())

		for {
			msgOut.Numbers, msgOut.K = generateRandomArray()
			computeOnServer(msgOut)
			time.Sleep(time.Second)
		}

	} else {
		msgOut.Numbers = config.Array
		msgOut.K = config.K
		computeOnServer(msgOut)
	}
}
